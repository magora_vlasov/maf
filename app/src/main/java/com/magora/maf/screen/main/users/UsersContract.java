package com.magora.maf.screen.main.users;

import com.magora.maf.screen.viewobject.UserVo;
import com.magora.maf.screen.base.mvppresenter.MvpPresenter;
import com.magora.maf.screen.base.mvpview.MvpView;
import com.magora.maf.screen.base.view.EmptyView;
import com.magora.maf.screen.base.view.MessageView;
import com.magora.maf.screen.base.view.ProgressView;
import com.magora.maf.screen.main.MainContract;

import java.util.List;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public interface UsersContract {

    interface View extends MvpView, ProgressView, EmptyView, MessageView {

        void setData(List<UserRecyclerObject> users);
    }

    interface Presenter extends MvpPresenter<View, MainContract.Router> {

        void onUserDetailsClick(UserVo user);
    }
}
