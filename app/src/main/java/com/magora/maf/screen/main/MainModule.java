package com.magora.maf.screen.main;

import com.magora.maf.model.datastorage.UserLoginStorage;

import dagger.Module;
import dagger.Provides;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
@Module
public class MainModule {

    @Provides
    protected MainContract.Presenter providePresenter() {
        return new MainPresenter();
    }

    @Provides
    protected UserLoginStorage provideUserLoginStorage() {
        return new UserLoginStorage();
    }
}
