package com.magora.maf.screen.main.about;

import com.magora.maf.application.di.holder.BaseComponentHolder;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public final class AboutComponentHolder extends BaseComponentHolder<AboutComponent> {
    private static final AboutComponentHolder instance = new AboutComponentHolder();

    private AboutComponentHolder() {
    }

    public static AboutComponentHolder getInstance() {
        return instance;
    }
}
