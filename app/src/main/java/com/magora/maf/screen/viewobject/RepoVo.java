package com.magora.maf.screen.viewobject;

import java.io.Serializable;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Value;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
@Value
@Builder(toBuilder = true)
@EqualsAndHashCode(of = "id")
public class RepoVo implements Serializable {
    Integer id;
    String name;
    UserVo owner;
    String description;
}
