package com.magora.maf.screen.base.view;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public interface MessageView {
    void showMessage(String message);
}
