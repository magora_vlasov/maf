package com.magora.maf.screen.user;

import com.magora.maf.model.datastorage.UserLoginStorage;
import com.magora.maf.model.usecase.GetUserUseCase;
import com.magora.maf.screen.base.mvppresenter.BaseLivePresenter;
import com.magora.maf.screen.viewobject.UserVo;
import com.magora.maf.screen.viewobject.UserVoMapper;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public class UserPresenter
        extends BaseLivePresenter<UserContract.View, UserContract.Router>
        implements UserContract.Presenter {
    private final UserLoginStorage userLoginStorage;
    private final GetUserUseCase getUserUseCase;

    public UserPresenter(UserLoginStorage userLoginStorage, GetUserUseCase getUserUseCase) {
        this.userLoginStorage = userLoginStorage;
        this.getUserUseCase = getUserUseCase;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        requestUserDetails();
    }

    @SuppressWarnings("CodeBlock2Expr")
    private void requestUserDetails() {
        handleUseCaseLifecycle(getUserUseCase);

        getUserUseCase.setData(userLoginStorage.get(""));

        getUserUseCase.setCallbacks(() -> {
                    consumeView(v -> {
                        v.setProgressViewEnabled(true);
                        v.setEmptyViewEnabled(false);
                    });
                },
                userDto -> {
                    consumeView(v -> {
                        // TODO: Empty view (and for users too)

                        final UserVo userVo = UserVoMapper.fromDto(userDto);

                        v.setProgressViewEnabled(false);
                        v.setEmptyViewEnabled(false);
                        v.setUser(userVo);
                    });
                },
                throwable -> {
                    consumeView(v -> {
                        v.setProgressViewEnabled(false);
                        v.setEmptyViewEnabled(true);
                        v.showMessage(throwable.getLocalizedMessage());
                    });
                }
        );

        getUserUseCase.execute();
    }
}
