package com.magora.maf.screen.main;

import com.magora.maf.application.di.holder.BaseComponentHolder;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public final class MainComponentHolder extends BaseComponentHolder<MainComponent> {
    private static final MainComponentHolder instance = new MainComponentHolder();

    private MainComponentHolder() {
    }

    public static MainComponentHolder getInstance() {
        return instance;
    }
}
