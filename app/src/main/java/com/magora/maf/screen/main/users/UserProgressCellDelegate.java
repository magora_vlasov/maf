package com.magora.maf.screen.main.users;

import com.magora.maf.screen.base.recycler.BaseProgressCellDelegate;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public final class UserProgressCellDelegate extends BaseProgressCellDelegate<UserRecyclerObject> {

    @Override
    public boolean is(UserRecyclerObject item) {
        return item.progress != null;
    }
}
