package com.magora.maf.screen.user;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.request.RequestOptions;
import com.magora.maf.R;
import com.magora.maf.screen.viewobject.UserVo;
import com.magora.maf.screen.base.fragment.BaseLiveFragment;

import butterknife.BindView;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public class UserFragment
        extends BaseLiveFragment<UserContract.Presenter, UserContract.View, UserContract.Router>
        implements UserContract.View {
    @BindView(R.id.avatar_image_view)
    protected ImageView avatarImageView;
    @BindView(R.id.name_text_view)
    protected TextView nameTextView;
    @BindView(R.id.empty_view)
    protected View emptyView;
    @BindView(R.id.progress_view)
    protected View progressView;

    public static UserFragment newInstance() {
        return new UserFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        UserComponentHolder.getInstance().getComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_user, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    //region UserContract.View
    @Override
    public void setUser(UserVo user) {
        getActivity().setTitle(user.getLogin());

        Glide.with(this)
                .load(user.getAvatarUrl())
                .apply(RequestOptions.bitmapTransform(new CircleCrop()))
                .into(avatarImageView);

        nameTextView.setText(user.getLogin());
    }

    @Override
    public void setProgressViewEnabled(boolean enabled) {
        progressView.setVisibility(enabled ? VISIBLE : GONE);
    }

    @Override
    public void setEmptyViewEnabled(boolean enabled) {
        emptyView.setVisibility(enabled ? VISIBLE : GONE);
    }

    @Override
    public void showMessage(String message) {
        Snackbar.make(avatarImageView, message, Snackbar.LENGTH_SHORT).show();
    }
    //endregion
}
