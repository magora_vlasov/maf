package com.magora.maf.screen.user;

import com.magora.maf.application.di.holder.BaseComponentHolder;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public final class UserComponentHolder extends BaseComponentHolder<UserComponent> {
    private static final UserComponentHolder instance = new UserComponentHolder();

    private UserComponentHolder() {
    }

    public static UserComponentHolder getInstance() {
        return instance;
    }
}
