package com.magora.maf.screen.main.users;

import com.magora.maf.screen.viewobject.ProgressVo;
import com.magora.maf.screen.viewobject.UserVo;

import io.reactivex.annotations.Nullable;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public final class UserRecyclerObject {
    @Nullable
    public final UserVo user;
    @Nullable
    public final ProgressVo progress;

    private UserRecyclerObject(UserVo user, ProgressVo progress) {
        this.user = user;
        this.progress = progress;
    }

    public static UserRecyclerObject newInstance(UserVo user) {
        return new UserRecyclerObject(user, null);
    }

    public static UserRecyclerObject newInstance(ProgressVo progress) {
        return new UserRecyclerObject(null, progress);
    }
}
