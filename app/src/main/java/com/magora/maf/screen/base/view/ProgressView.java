package com.magora.maf.screen.base.view;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public interface ProgressView {
    void setProgressViewEnabled(boolean enabled);
}
