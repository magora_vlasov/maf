package com.magora.maf.screen.viewobject;

import com.magora.maf.model.dataobject.dto.RepoDto;

import java.util.ArrayList;
import java.util.List;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public final class RepoVoMapper {

    public static RepoVo fromDto(RepoDto repoDto) {
        return RepoVo.builder()
                .id(repoDto.getId())
                .name(repoDto.getName())
                .owner(UserVoMapper.fromDto(repoDto.getOwner()))
                .description(repoDto.getDescription())
                .build();
    }

    public static List<RepoVo> fromDto(List<RepoDto> repoDtos) {
        final List<RepoVo> repoVos = new ArrayList<>();
        for (RepoDto repoDto : repoDtos) {
            repoVos.add(fromDto(repoDto));
        }
        return repoVos;
    }

    public static RepoDto toDto(RepoVo repoVo) {
        return RepoDto.builder()
                .id(repoVo.getId())
                .name(repoVo.getName())
                .owner(UserVoMapper.toDto(repoVo.getOwner()))
                .description(repoVo.getDescription())
                .build();
    }

    public static List<RepoDto> toDto(List<RepoVo> repoVos) {
        final List<RepoDto> repoDtos = new ArrayList<>();
        for (RepoVo repoVo : repoVos) {
            repoDtos.add(toDto(repoVo));
        }
        return repoDtos;
    }
}
