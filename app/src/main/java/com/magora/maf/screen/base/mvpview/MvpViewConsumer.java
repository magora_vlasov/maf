package com.magora.maf.screen.base.mvpview;

import io.reactivex.functions.Consumer;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public interface MvpViewConsumer<V extends MvpView> {
    void consumeView(final Consumer<V> consumer);
}
