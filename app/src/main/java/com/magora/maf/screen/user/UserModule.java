package com.magora.maf.screen.user;

import com.magora.maf.model.dataprovider.UsersDataProvider;
import com.magora.maf.model.datastorage.UserLoginStorage;
import com.magora.maf.model.usecase.GetUserUseCase;

import dagger.Module;
import dagger.Provides;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
@Module
public class UserModule {

    @Provides
    protected UserContract.Presenter providePresenter(UserLoginStorage userLoginStorage,
                                                      GetUserUseCase getUserUseCase) {
        return new UserPresenter(userLoginStorage, getUserUseCase);
    }

    @Provides
    protected UserLoginStorage provideUserLoginStorage() {
        return new UserLoginStorage();
    }

    @Provides
    protected GetUserUseCase provideGetUserUseCase(UsersDataProvider usersDataProvider) {
        return new GetUserUseCase(usersDataProvider);
    }
}
