package com.magora.maf.screen.user;

import com.magora.maf.application.di.AppComponent;
import com.magora.maf.application.di.scope.ActivityScope;

import dagger.Component;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
@ActivityScope
@Component(dependencies = AppComponent.class, modules = UserModule.class)
public interface UserComponent {

    void inject(UserFragment fragment);
}
