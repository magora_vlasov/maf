package com.magora.maf.screen.base.mvppresenter;

import com.magora.maf.screen.base.mvprouter.MvpRouter;
import com.magora.maf.screen.base.mvpview.MvpView;

import io.reactivex.functions.Consumer;
import timber.log.Timber;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public abstract class BasePresenter<V extends MvpView, R extends MvpRouter>
        implements MvpPresenter<V, R> {
    private V view;
    private R router;

    @Override
    public void onCreate() {
        // Do nothing
    }

    @Override
    public void onAttach(final V view, R router) {
        this.view = view;
        this.router = router;
    }

    @Override
    public void onDetach() {
        this.view = null;
        this.router = null;
    }

    @Override
    public void onDestroy() {
        // Do nothing
    }

    @Override
    public void consumeView(Consumer<V> consumer) {
        try {
            if (view == null) {
                Timber.w("MvpView is null");
            } else {
                consumer.accept(view);
            }
        } catch (Exception e) {
            Timber.w(e);
        }
    }

    @Override
    public void consumeRouter(Consumer<R> consumer) {
        try {
            if (router == null) {
                Timber.w("MvpRouter is null");
            } else {
                consumer.accept(router);
            }
        } catch (Exception e) {
            Timber.w(e);
        }
    }
}
