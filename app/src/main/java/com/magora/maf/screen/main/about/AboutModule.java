package com.magora.maf.screen.main.about;

import dagger.Module;
import dagger.Provides;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
@Module
public class AboutModule {

    @Provides
    protected AboutContract.Presenter providePresenter() {
        return new AboutPresenter();
    }
}
