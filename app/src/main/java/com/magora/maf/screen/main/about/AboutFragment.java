package com.magora.maf.screen.main.about;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.magora.maf.R;
import com.magora.maf.application.di.AppComponentHolder;
import com.magora.maf.screen.base.fragment.BaseLiveFragment;
import com.magora.maf.screen.main.MainContract;
import com.magora.maf.screen.base.OnItemReselectedListener;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public class AboutFragment
        extends BaseLiveFragment<AboutContract.Presenter, AboutContract.View, MainContract.Router>
        implements AboutContract.View, OnItemReselectedListener {

    public static AboutFragment newInstance() {
        return new AboutFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        AboutComponentHolder.getInstance().bindComponent(
                DaggerAboutComponent.builder()
                        .appComponent(AppComponentHolder.getInstance().getComponent())
                        .build()
        );
        AboutComponentHolder.getInstance().getComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_about, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        AboutComponentHolder.getInstance().unbindComponent();
    }

    //region OnItemReselectedListener
    @Override
    public void onItemReselected() {
        Toast.makeText(getContext(), "Reselected", Toast.LENGTH_SHORT).show();
    }
    //endregion
}
