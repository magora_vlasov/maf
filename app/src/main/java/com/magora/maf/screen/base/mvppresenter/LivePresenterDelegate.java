package com.magora.maf.screen.base.mvppresenter;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.magora.maf.screen.base.mvprouter.MvpRouter;
import com.magora.maf.screen.base.mvpview.MvpView;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public interface LivePresenterDelegate<P extends MvpPresenter<V, R>, V extends MvpView, R extends MvpRouter>
        extends MvpPresenterConsumer<P, V, R> {

    void onCreate(@Nullable Bundle savedInstanceState);

    void onStart();

    void onStop();

    void onDestroy();
}
