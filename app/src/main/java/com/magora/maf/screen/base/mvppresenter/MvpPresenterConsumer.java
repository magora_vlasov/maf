package com.magora.maf.screen.base.mvppresenter;

import com.magora.maf.screen.base.mvprouter.MvpRouter;
import com.magora.maf.screen.base.mvpview.MvpView;

import io.reactivex.functions.Consumer;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public interface MvpPresenterConsumer<P extends MvpPresenter<V, R>, V extends MvpView, R extends MvpRouter> {
    void consumePresenter(final Consumer<P> consumer);
}
