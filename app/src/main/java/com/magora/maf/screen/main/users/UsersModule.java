package com.magora.maf.screen.main.users;

import com.magora.maf.model.dataprovider.UsersDataProvider;
import com.magora.maf.model.usecase.GetUsersUseCase;

import dagger.Module;
import dagger.Provides;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
@Module
public class UsersModule {

    @Provides
    protected UsersContract.Presenter providePresenter(GetUsersUseCase getUsersUseCase) {
        return new UsersPresenter(getUsersUseCase);
    }

    @Provides
    protected GetUsersUseCase provideGetUsersUseCase(UsersDataProvider usersDataProvider) {
        return new GetUsersUseCase(usersDataProvider);
    }
}
