package com.magora.maf.screen.main.users;

import com.magora.maf.application.di.AppComponent;
import com.magora.maf.application.di.scope.FragmentScope;

import dagger.Component;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
@FragmentScope
@Component(dependencies = AppComponent.class, modules = UsersModule.class)
public interface UsersComponent {

    void inject(UsersFragment fragment);
}
