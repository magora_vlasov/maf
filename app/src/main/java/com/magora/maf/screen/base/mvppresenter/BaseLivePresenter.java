package com.magora.maf.screen.base.mvppresenter;

import com.magora.maf.model.usecase.base.LiveUseCase;
import com.magora.maf.model.usecase.base.UseCase;
import com.magora.maf.screen.base.mvprouter.MvpRouter;
import com.magora.maf.screen.base.mvpview.MvpView;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Flowable;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public abstract class BaseLivePresenter<V extends MvpView, R extends MvpRouter>
        extends BasePresenter<V, R> {
    private List<LiveUseCase> liveUseCases = new ArrayList<>();

    @SuppressWarnings("unused")
    protected void handleUseCaseLifecycle(LiveUseCase liveUseCase) {
        liveUseCases.add(liveUseCase);
    }

    //region MvpPresenter lifecycle
    @Override
    public void onCreate() {
        super.onCreate();
        // Do nothing
    }

    @Override
    public void onAttach(V view, R router) {
        super.onAttach(view, router);
        Flowable.fromIterable(liveUseCases).subscribe(LiveUseCase::pushLastEvent);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        // Do nothing
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Flowable.fromIterable(liveUseCases).subscribe(UseCase::abort);
    }
    //endregion
}
