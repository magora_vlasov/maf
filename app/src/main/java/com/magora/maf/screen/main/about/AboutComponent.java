package com.magora.maf.screen.main.about;

import com.magora.maf.application.di.AppComponent;
import com.magora.maf.application.di.scope.FragmentScope;

import dagger.Component;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
@FragmentScope
@Component(dependencies = AppComponent.class, modules = AboutModule.class)
public interface AboutComponent {

    void inject(AboutFragment fragment);
}
