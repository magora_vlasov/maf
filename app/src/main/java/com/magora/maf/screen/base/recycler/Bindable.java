package com.magora.maf.screen.base.recycler;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public interface Bindable<T> {
    void bind(T item);
}
