package com.magora.maf.screen.viewobject;

import lombok.Value;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
@Value
public class ProgressVo {
}
