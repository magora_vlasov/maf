package com.magora.maf.screen.base;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public interface OnItemReselectedListener {
    void onItemReselected();
}
