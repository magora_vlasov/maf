package com.magora.maf.screen.main;

import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;

import com.magora.maf.R;
import com.magora.maf.application.di.AppComponentHolder;
import com.magora.maf.application.util.RoutingUtils;
import com.magora.maf.model.datastorage.UserLoginStorage;
import com.magora.maf.screen.base.OnItemReselectedListener;
import com.magora.maf.screen.base.activity.BaseLiveActivity;
import com.magora.maf.screen.main.about.AboutFragment;
import com.magora.maf.screen.main.repos.ReposFragment;
import com.magora.maf.screen.main.users.UsersFragment;
import com.magora.maf.screen.user.UserActivity;

import javax.inject.Inject;

import butterknife.BindView;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public class MainActivity
        extends BaseLiveActivity<MainContract.Presenter, MainContract.View, MainContract.Router>
        implements MainContract.View, MainContract.Router {
    @BindView(R.id.toolbar)
    protected Toolbar toolbar;
    @BindView(R.id.bottom_navigation_view)
    protected BottomNavigationView bottomNavigationView;

    @Inject
    protected UserLoginStorage userLoginStorage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        initDaggerComponent();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews(savedInstanceState);
        initListeners();
    }

    private void initDaggerComponent() {
        MainComponentHolder.getInstance().bindComponent(
                DaggerMainComponent.builder()
                        .appComponent(AppComponentHolder.getInstance().getComponent())
                        .build()
        );
        MainComponentHolder.getInstance().getComponent().inject(this);
    }

    private void initViews(Bundle savedInstanceState) {
        setSupportActionBar(toolbar);

        RoutingUtils.showFragment(
                this,
                savedInstanceState,
                R.id.container_view_group,
                UsersFragment.newInstance()
        );
    }

    @SuppressWarnings("CodeBlock2Expr")
    private void initListeners() {
        bottomNavigationView.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.item_users:
                    consumePresenter(MainContract.Presenter::onUsersSelected);
                    return true;
                case R.id.item_repos:
                    consumePresenter(MainContract.Presenter::onReposSelected);
                    return true;
                case R.id.item_about:
                    consumePresenter(MainContract.Presenter::onAboutSelected);
                    return true;
                default:
                    return false;
            }
        });

        bottomNavigationView.setOnNavigationItemReselectedListener(item -> {
            consumePresenter(MainContract.Presenter::onItemReselected);
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        MainComponentHolder.getInstance().unbindComponent();
    }

    //region MainContract.View
    @Override
    public void pushReselectedEvent() {
        final Fragment fragment =
                getSupportFragmentManager().findFragmentById(R.id.container_view_group);

        if (fragment instanceof OnItemReselectedListener) {
            ((OnItemReselectedListener) fragment).onItemReselected();
        } else {
            throw new IllegalStateException(
                    "Child fragments must implement OnItemReselectedListener interface");
        }
    }
    //endregion

    //region MainContract.Router
    @Override
    public void routeToUsers() {
        RoutingUtils.showFragment(
                this,
                R.id.container_view_group,
                UsersFragment.newInstance()
        );
    }

    @Override
    public void routeToRepos() {
        RoutingUtils.showFragment(
                this,
                R.id.container_view_group,
                ReposFragment.newInstance()
        );
    }

    @Override
    public void routeToAbout() {
        RoutingUtils.showFragment(
                this,
                R.id.container_view_group,
                AboutFragment.newInstance()
        );
    }

    @Override
    public void routeToUserDetails(String userLogin) {
        userLoginStorage.put(userLogin);
        RoutingUtils.startActivity(this, UserActivity.class);
    }
    //endregion
}
