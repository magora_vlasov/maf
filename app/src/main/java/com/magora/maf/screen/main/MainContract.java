package com.magora.maf.screen.main;

import com.magora.maf.screen.base.mvppresenter.MvpPresenter;
import com.magora.maf.screen.base.mvprouter.MvpRouter;
import com.magora.maf.screen.base.mvpview.MvpView;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public interface MainContract {

    interface Presenter extends MvpPresenter<View, Router> {

        void onUsersSelected();

        void onReposSelected();

        void onAboutSelected();

        void onItemReselected();
    }

    interface View extends MvpView {
        void pushReselectedEvent();
    }

    interface Router extends MvpRouter {

        void routeToUsers();

        void routeToRepos();

        void routeToAbout();

        void routeToUserDetails(String userLogin);
    }
}
