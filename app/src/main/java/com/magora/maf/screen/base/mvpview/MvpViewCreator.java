package com.magora.maf.screen.base.mvpview;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public interface MvpViewCreator<V extends MvpView> {
    V createView();
}
