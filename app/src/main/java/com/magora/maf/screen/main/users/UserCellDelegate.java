package com.magora.maf.screen.main.users;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.request.RequestOptions;
import com.magora.maf.R;
import com.magora.maf.screen.viewobject.UserVo;
import com.magora.maf.screen.base.recycler.BaseCellDelegate;
import com.magora.maf.screen.base.recycler.BaseViewHolder;
import com.magora.maf.screen.base.recycler.OnCellDelegateClickListener;

import butterknife.BindView;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public final class UserCellDelegate extends BaseCellDelegate<UserRecyclerObject> {
    private OnCellDelegateClickListener<UserVo> userClickListener;

    public void setUserClickListener(OnCellDelegateClickListener<UserVo> userClickListener) {
        this.userClickListener = userClickListener;
    }

    @Override
    public boolean is(UserRecyclerObject item) {
        return item.user != null;
    }

    @Override
    public BaseViewHolder<UserRecyclerObject> holder(ViewGroup parent) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View view = inflater.inflate(R.layout.item_user, parent, false);
        return new UserViewHolder(view);
    }

    protected class UserViewHolder extends BaseViewHolder<UserRecyclerObject> {
        @BindView(R.id.avatar_image_view)
        protected ImageView avatarImageView;
        @BindView(R.id.name_text_view)
        protected TextView nameTextView;

        public UserViewHolder(View itemView) {
            super(itemView);
        }

        @SuppressWarnings("CodeBlock2Expr")
        @Override
        public void bind(UserRecyclerObject item) {
            final UserVo user = item.user;

            Glide.with(itemView)
                    .load(user.getAvatarUrl())
                    .apply(RequestOptions.bitmapTransform(new CircleCrop()))
                    .into(avatarImageView);

            nameTextView.setText(user.getLogin());

            if (userClickListener != null) {
                itemView.setOnClickListener(v -> {
                    userClickListener.onCellDelegateClick(itemView, getAdapterPosition(), user);
                });
            }
        }
    }
}
