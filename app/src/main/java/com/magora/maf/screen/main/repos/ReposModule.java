package com.magora.maf.screen.main.repos;

import com.magora.maf.model.dataprovider.ReposDataProvider;
import com.magora.maf.model.usecase.GetReposUseCase;

import dagger.Module;
import dagger.Provides;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
@Module
public class ReposModule {

    @Provides
    protected ReposContract.Presenter providePresenter(GetReposUseCase getReposUseCase) {
        return new ReposPresenter(getReposUseCase);
    }

    @Provides
    protected GetReposUseCase provideGetReposUseCase(ReposDataProvider reposDataProvider) {
        return new GetReposUseCase(reposDataProvider);
    }
}
