package com.magora.maf.screen.base.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.magora.maf.screen.base.mvppresenter.LivePresenterDelegate;
import com.magora.maf.screen.base.mvppresenter.LivePresenterDelegateImpl;
import com.magora.maf.screen.base.mvppresenter.MvpPresenter;
import com.magora.maf.screen.base.mvppresenter.MvpPresenterConsumer;
import com.magora.maf.screen.base.mvppresenter.MvpPresenterCreator;
import com.magora.maf.screen.base.mvprouter.MvpRouter;
import com.magora.maf.screen.base.mvpview.MvpView;
import com.magora.maf.screen.base.mvpview.MvpViewCreator;

import javax.inject.Inject;

import dagger.Lazy;
import io.reactivex.functions.Consumer;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public abstract class BaseLiveActivity<P extends MvpPresenter<V, R>, V extends MvpView, R extends MvpRouter>
        extends BaseActivity
        implements MvpViewCreator<V>, MvpPresenterCreator<P, V, R>, MvpPresenterConsumer<P, V, R> {
    private LivePresenterDelegate<P, V, R> livePresenterDelegate;

    //region MVP creation section
    @Inject
    protected Lazy<P> presenter;

    @Override
    public P createPresenter() {
        return presenter.get();
    }

    @Override
    public V createView() {
        //noinspection unchecked
        return (V) this;
    }
    //endregion

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        livePresenterDelegate = new LivePresenterDelegateImpl<>(this, this, this);
        livePresenterDelegate.onCreate(savedInstanceState);
    }

    @Override
    protected void onStart() {
        super.onStart();
        livePresenterDelegate.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        livePresenterDelegate.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        livePresenterDelegate.onDestroy();
    }

    @Override
    public void consumePresenter(Consumer<P> consumer) {
        livePresenterDelegate.consumePresenter(consumer);
    }
}
