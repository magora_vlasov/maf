package com.magora.maf.screen.base.mvppresenter;

import com.magora.maf.screen.base.mvprouter.MvpRouter;
import com.magora.maf.screen.base.mvprouter.MvpRouterConsumer;
import com.magora.maf.screen.base.mvpview.MvpView;
import com.magora.maf.screen.base.mvpview.MvpViewConsumer;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public interface MvpPresenter<V extends MvpView, R extends MvpRouter>
        extends MvpViewConsumer<V>, MvpRouterConsumer<R> {

    void onCreate();

    void onAttach(final V view, final R router);

    void onDetach();

    void onDestroy();
}
