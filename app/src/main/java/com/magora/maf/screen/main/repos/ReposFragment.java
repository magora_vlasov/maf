package com.magora.maf.screen.main.repos;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.magora.maf.R;
import com.magora.maf.application.di.AppComponentHolder;
import com.magora.maf.screen.base.fragment.BaseLiveFragment;
import com.magora.maf.screen.main.MainContract;
import com.magora.maf.screen.base.OnItemReselectedListener;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public class ReposFragment
        extends BaseLiveFragment<ReposContract.Presenter, ReposContract.View, MainContract.Router>
        implements ReposContract.View, OnItemReselectedListener {

    public static ReposFragment newInstance() {
        return new ReposFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        ReposComponentHolder.getInstance().bindComponent(
                DaggerReposComponent.builder()
                        .appComponent(AppComponentHolder.getInstance().getComponent())
                        .build()
        );
        ReposComponentHolder.getInstance().getComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_repos, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        ReposComponentHolder.getInstance().unbindComponent();
    }

    //region OnItemReselectedListener
    @Override
    public void onItemReselected() {
        Toast.makeText(getContext(), "Reselected", Toast.LENGTH_SHORT).show();
    }
    //endregion
}
