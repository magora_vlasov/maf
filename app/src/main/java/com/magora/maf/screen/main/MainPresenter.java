package com.magora.maf.screen.main;

import com.magora.maf.screen.base.mvppresenter.BaseLivePresenter;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public class MainPresenter
        extends BaseLivePresenter<MainContract.View, MainContract.Router>
        implements MainContract.Presenter {

    //region MainContract.Presenter
    @Override
    public void onUsersSelected() {
        consumeRouter(MainContract.Router::routeToUsers);
    }

    @Override
    public void onReposSelected() {
        consumeRouter(MainContract.Router::routeToRepos);
    }

    @Override
    public void onAboutSelected() {
        consumeRouter(MainContract.Router::routeToAbout);
    }

    @Override
    public void onItemReselected() {
        consumeView(MainContract.View::pushReselectedEvent);
    }
    //endregion
}
