package com.magora.maf.screen.base.mvppresenter;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.magora.maf.application.manager.PresenterManager;
import com.magora.maf.screen.base.mvprouter.MvpRouter;
import com.magora.maf.screen.base.mvpview.MvpView;
import com.magora.maf.screen.base.mvpview.MvpViewCreator;

import io.reactivex.functions.Consumer;
import timber.log.Timber;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public final class LivePresenterDelegateImpl<P extends MvpPresenter<V, R>, V extends MvpView, R extends MvpRouter>
        implements LivePresenterDelegate<P, V, R> {
    private final Activity activity;
    private final PresenterManager presenterManager;
    private final Class presenterKey;
    private V view;
    private P presenter;
    private R router;

    @SuppressWarnings("unused")
    public LivePresenterDelegateImpl(@NonNull final Activity activity,
                                     @NonNull final MvpViewCreator<V> viewCreator,
                                     @NonNull final MvpPresenterCreator<P, V, R> presenterCreator) {
        this.activity = activity;
        this.presenterManager = PresenterManager.getInstance();
        this.presenterKey = activity.getClass();
        this.view = viewCreator.createView();

        initPresenter(presenterCreator);
        initRouter(activity);
    }

    @SuppressWarnings("unused")
    public LivePresenterDelegateImpl(@NonNull final Fragment fragment,
                                     @NonNull final MvpViewCreator<V> viewCreator,
                                     @NonNull final MvpPresenterCreator<P, V, R> presenterCreator) {
        this.activity = fragment.getActivity();
        this.presenterManager = PresenterManager.getInstance();
        this.presenterKey = fragment.getClass();
        this.view = viewCreator.createView();

        initPresenter(presenterCreator);
        initRouter(activity);
    }

    @SuppressWarnings("unused")
    public LivePresenterDelegateImpl(@NonNull final android.app.Fragment fragment,
                                     @NonNull final MvpViewCreator<V> viewCreator,
                                     @NonNull final MvpPresenterCreator<P, V, R> presenterCreator) {
        this.activity = fragment.getActivity();
        this.presenterManager = PresenterManager.getInstance();
        this.presenterKey = fragment.getClass();
        this.view = viewCreator.createView();

        initPresenter(presenterCreator);
        initRouter(activity);
    }

    private void initPresenter(@NonNull final MvpPresenterCreator<P, V, R> presenterCreator) {
        if (presenterManager.contains(presenterKey)) {
            //noinspection unchecked
            presenter = presenterManager.get(presenterKey);
        } else {
            presenter = presenterCreator.createPresenter();
            presenterManager.add(presenterKey, presenter);
        }
    }

    private void initRouter(@NonNull final Activity activity) {
        try {
            //noinspection unchecked
            this.router = (R) activity;
        } catch (ClassCastException e) {
            throw new IllegalStateException("Activity must implement <ScreenContract>.Router interface");
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            consumePresenter(MvpPresenter::onCreate);
        }
    }

    @Override
    public void onStart() {
        consumePresenter(p -> p.onAttach(view, router));
    }

    @Override
    public void onStop() {
        if (activity.isFinishing()) {
            presenterManager.remove(presenterKey);
        }
        consumePresenter(MvpPresenter::onDetach);
    }

    @Override
    public void onDestroy() {
        if (activity.isFinishing()) {
            consumePresenter(MvpPresenter::onDestroy);
        }
    }

    @Override
    public void consumePresenter(Consumer<P> consumer) {
        try {
            if (presenter == null) {
                Timber.w("MvpPresenter is null");
            } else {
                consumer.accept(presenter);
            }
        } catch (Exception e) {
            Timber.w(e);
        }
    }
}
