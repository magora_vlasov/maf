package com.magora.maf.screen.base.recycler;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.magora.maf.R;
import com.magora.maf.screen.base.recycler.BaseCellDelegate;
import com.magora.maf.screen.base.recycler.BaseViewHolder;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public abstract class BaseProgressCellDelegate<T> extends BaseCellDelegate<T> {

    @Override
    public BaseViewHolder<T> holder(ViewGroup parent) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View view = inflater.inflate(R.layout.item_progress, parent, false);
        return new ProgressViewHolder(view);
    }

    public class ProgressViewHolder extends BaseViewHolder<T> {

        public ProgressViewHolder(View itemView) {
            super(itemView);
        }

        @Override
        public void bind(T item) {
            // Do nothing.
        }
    }
}
