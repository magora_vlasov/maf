package com.magora.maf.model.datastorage.base;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public interface SingleDataStorage<T> {

    @Nullable
    T get();

    @NonNull
    T get(@NonNull T defaultValue);

    void put(@NonNull T value);

    void remove();
}
