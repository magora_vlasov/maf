package com.magora.maf.model.datasource.repos;

import com.magora.maf.model.dataobject.dto.RepoDto;

import java.util.Collections;
import java.util.List;

import io.reactivex.Flowable;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public final class ReposCacheDataSource implements ReposDataSource {

    @Override
    public Flowable<List<RepoDto>> getRepos() {
        return Flowable.fromCallable(Collections::emptyList);
    }

    @Override
    public Flowable<List<RepoDto>> getReposForUser(String login) {
        return Flowable.fromCallable(Collections::emptyList);
    }
}
