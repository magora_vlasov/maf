package com.magora.maf.model.usecase.base;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public abstract class BaseLiveUseCase<In, Out> implements LiveUseCase<In, Out> {
    protected Action progressCallback;
    protected Consumer<Out> outputDataCallback;
    protected Consumer<Throwable> errorCallback;

    protected In inputData;
    protected Out outputData;
    protected Throwable throwable;

    protected Disposable disposable; // TODO: Адаптировать под множественные запросы

    //region UseCase
    @Override
    public void execute() {
        if (isDisposed()) {
            pushProgressEvent();
            try {
                executeOperation();
            } catch (Exception e) {
                handleOnError(e);
            }
        }
    }

    @Override
    public void forceExecute() {
        pushProgressEvent();
        abort();
        try {
            executeOperation();
        } catch (Exception e) {
            handleOnError(e);
        }
    }

    protected abstract void executeOperation() throws Exception;

    @Override
    public void abort() {
        inputData = null;
        outputData = null;
        throwable = null;

        if (disposable != null) {
            disposable.dispose();
            disposable = null;
        }
    }

    @Override
    public void setData(@NonNull In inputData) {
        this.inputData = inputData;
    }

    @Override
    public void setCallbacks(@Nullable Action progressCallback,
                             @Nullable Consumer<Out> outputDataCallback,
                             @Nullable Consumer<Throwable> errorCallback) {
        this.progressCallback = progressCallback;
        this.outputDataCallback = outputDataCallback;
        this.errorCallback = errorCallback;
    }
    //endregion

    //region LiveUseCase
    @Override
    public void pushProgressEvent() {
        try {
            progressCallback.run();
        } catch (Exception e) {
            Timber.w(e);
        }
    }

    @Override
    public void pushOutputDataEvent() {
        try {
            outputDataCallback.accept(outputData);
        } catch (Exception e) {
            Timber.w(e);
        }
    }

    @Override
    public void pushErrorEvent() {
        try {
            errorCallback.accept(throwable);
        } catch (Exception e) {
            Timber.w(e);
        }
    }

    @Override
    public void pushLastEvent() {
        if (!isDisposed()) {
            pushProgressEvent();
            return;
        }

        if (outputData != null) {
            pushOutputDataEvent();
            return;
        }

        if (throwable != null) {
            pushErrorEvent();
        }
    }
    //endregion

    //region Protected block
    protected boolean isDisposed() {
        return disposable == null || disposable.isDisposed();
    }

    protected void executeOperation(Flowable<Out> flowable) {
        disposable = flowable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::handleOnNext, this::handleOnError);
    }

    protected void executeOperation(Completable completable) {
        disposable = completable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::pushOutputDataEvent, this::handleOnError);
    }

    protected void executeOperation(Single<Out> single) {
        disposable = single
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::handleOnNext, this::handleOnError);
    }

    protected void handleOnNext(Out outputData) {
        this.outputData = outputData;
        pushOutputDataEvent();
    }

    protected void handleOnError(Throwable throwable) {
        this.throwable = throwable;
        pushErrorEvent();
    }
    //endregion
}
