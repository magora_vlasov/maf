package com.magora.maf.model.datasource.users;

import com.magora.maf.model.dataobject.dto.UserDto;
import com.magora.maf.model.dataobject.mocker.UserMocker;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public final class UsersMockDataSource implements UsersDataSource {

    @Override
    public Flowable<List<UserDto>> getUsers() {
        return Flowable.fromCallable(UserMocker::getUserDtoList);
    }

    @Override
    public Flowable<UserDto> getUser(String login) {
        return Flowable.fromCallable(UserMocker::getUserDto);
    }
}
