package com.magora.maf.model.datastorage.base;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public interface DataStorage<T> {

    @Nullable
    T get(@NonNull String key);

    @NonNull
    T get(@NonNull String key, @NonNull T defaultValue);

    void put(@NonNull String key, @NonNull T value);

    void remove(@NonNull String key);
}
