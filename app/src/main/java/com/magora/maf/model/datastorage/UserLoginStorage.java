package com.magora.maf.model.datastorage;

import com.magora.maf.model.datastorage.base.PaperSingleDataStorage;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public final class UserLoginStorage extends PaperSingleDataStorage<String> {
    private static final String TAG = UserLoginStorage.class.getSimpleName();
    private static final String KEY_LOGIN = TAG + "_KEY_LOGIN";

    @Override
    protected String getKey() {
        return KEY_LOGIN;
    }
}
