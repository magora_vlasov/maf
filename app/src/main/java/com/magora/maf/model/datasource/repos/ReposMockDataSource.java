package com.magora.maf.model.datasource.repos;

import com.magora.maf.model.dataobject.dto.RepoDto;
import com.magora.maf.model.dataobject.mocker.RepoMocker;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public final class ReposMockDataSource implements ReposDataSource {

    @Override
    public Flowable<List<RepoDto>> getRepos() {
        return Flowable.fromCallable(RepoMocker::getRepos);
    }

    @Override
    public Flowable<List<RepoDto>> getReposForUser(String login) {
        return Flowable.fromCallable(RepoMocker::getRepos);
    }
}
