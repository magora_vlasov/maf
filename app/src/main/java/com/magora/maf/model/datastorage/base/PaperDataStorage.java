package com.magora.maf.model.datastorage.base;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import io.paperdb.Paper;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public abstract class PaperDataStorage<T> implements DataStorage<T> {

    @Nullable
    @Override
    public T get(@NonNull String key) {
        return Paper.book().read(key);
    }

    @NonNull
    @Override
    public T get(@NonNull String key, @NonNull T defaultValue) {
        return Paper.book().read(key, defaultValue);
    }

    @Override
    public void put(@NonNull String key, @NonNull T value) {
        Paper.book().write(key, value);
    }

    @Override
    public void remove(@NonNull String key) {
        Paper.book().delete(key);
    }
}
