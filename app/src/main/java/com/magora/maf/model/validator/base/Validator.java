package com.magora.maf.model.validator.base;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public interface Validator<T> {

    boolean isValid(T value);

    String getDescription();
}
