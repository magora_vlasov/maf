package com.magora.maf.model.usecase;

import com.magora.maf.model.dataobject.dto.RepoDto;
import com.magora.maf.screen.viewobject.RepoVo;
import com.magora.maf.model.dataprovider.ReposDataProvider;
import com.magora.maf.model.usecase.base.BaseLiveUseCase;

import java.util.List;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public final class GetReposForUserUseCase extends BaseLiveUseCase<String, List<RepoDto>> {
    private final ReposDataProvider reposDataProvider;

    public GetReposForUserUseCase(ReposDataProvider reposDataProvider) {
        this.reposDataProvider = reposDataProvider;
    }

    @Override
    protected void executeOperation() throws Exception {
        // TODO: Очень неочевидно сетать в UseCase какую-то абстрактную String. Подумать, что тут можно сделать.
        executeOperation(reposDataProvider.getReposForUser(inputData));
    }
}
