package com.magora.maf.model.usecase;

import com.magora.maf.model.dataobject.dto.RepoDto;
import com.magora.maf.screen.viewobject.RepoVo;
import com.magora.maf.model.dataprovider.ReposDataProvider;
import com.magora.maf.model.usecase.base.BaseLiveUseCase;

import java.util.List;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public final class GetReposUseCase extends BaseLiveUseCase<Void, List<RepoDto>> {
    private final ReposDataProvider reposDataProvider;

    public GetReposUseCase(ReposDataProvider reposDataProvider) {
        this.reposDataProvider = reposDataProvider;
    }

    @Override
    protected void executeOperation() throws Exception {
        executeOperation(reposDataProvider.getRepos());
    }
}
