package com.magora.maf.model.datastorage.base;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import io.paperdb.Paper;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public abstract class PaperSingleDataStorage<T> implements SingleDataStorage<T> {

    protected abstract String getKey();

    @Nullable
    @Override
    public T get() {
        return Paper.book().read(getKey());
    }

    @NonNull
    @Override
    public T get(@NonNull T defaultValue) {
        return Paper.book().read(getKey(), defaultValue);
    }

    @Override
    public void put(@NonNull T value) {
        Paper.book().write(getKey(), value);
    }

    @Override
    public void remove() {
        Paper.book().delete(getKey());
    }
}
