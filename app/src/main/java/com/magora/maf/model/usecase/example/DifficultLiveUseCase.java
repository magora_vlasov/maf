package com.magora.maf.model.usecase.example;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.magora.maf.model.usecase.base.BaseLiveUseCase;

import java.util.concurrent.TimeUnit;

import io.reactivex.Flowable;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public final class DifficultLiveUseCase
        extends BaseLiveUseCase<DifficultLiveUseCase.InputDataWrapper, DifficultLiveUseCase.OutputDataWrapper> {

    @Override
    public void executeOperation() {
        final Flowable<OutputDataWrapper> f = Flowable.timer(3, TimeUnit.SECONDS)
                .map(aLong -> new OutputDataWrapper("0", 1));

        executeOperation(f);
    }

    public static class InputDataWrapper {
        @NonNull
        public final String name;
        @Nullable
        public final Integer age;
        @NonNull
        public final Gender gender;

        public InputDataWrapper(@NonNull String name,
                                @Nullable Integer age,
                                @NonNull Gender gender) {
            this.name = name;
            this.age = age;
            this.gender = gender;
        }

        public enum Gender {MALE, FEMALE}
    }

    public static class OutputDataWrapper {
        @NonNull
        public final String string;
        @NonNull
        public final Integer integer;

        public OutputDataWrapper(@NonNull String string,
                                 @NonNull Integer integer) {
            this.string = string;
            this.integer = integer;
        }
    }
}
