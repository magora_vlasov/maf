package com.magora.maf.model.validator;

import android.util.Patterns;

import com.magora.maf.R;
import com.magora.maf.application.manager.ResExtractor;
import com.magora.maf.model.validator.base.Validator;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public class EmailValidator implements Validator<String> {

    @Override
    public boolean isValid(String value) {
        return Patterns.EMAIL_ADDRESS.matcher(value).matches();
    }

    @Override
    public String getDescription() {
        return ResExtractor.getInstance().getString(R.string.error_validation_email);
    }
}
