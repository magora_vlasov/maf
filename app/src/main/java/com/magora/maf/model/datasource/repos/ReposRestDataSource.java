package com.magora.maf.model.datasource.repos;

import com.magora.maf.application.rest.RestClient;
import com.magora.maf.model.dataobject.dto.RepoDto;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public final class ReposRestDataSource implements ReposDataSource {
    private final RestClient restClient;

    public ReposRestDataSource(RestClient restClient) {
        this.restClient = restClient;
    }

    @Override
    public Flowable<List<RepoDto>> getRepos() {
        return restClient.getRepos();
    }

    @Override
    public Flowable<List<RepoDto>> getReposForUser(String login) {
        return restClient.getReposForUser(login);
    }
}
