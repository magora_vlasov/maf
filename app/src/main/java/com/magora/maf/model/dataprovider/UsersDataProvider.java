package com.magora.maf.model.dataprovider;

import com.magora.maf.model.dataobject.dto.UserDto;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public interface UsersDataProvider {

    Flowable<List<UserDto>> getUsers();

    Flowable<UserDto> getUser(String login);
}
