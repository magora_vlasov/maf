package com.magora.maf.model.dataprovider;

import com.magora.maf.model.dataobject.dto.RepoDto;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public interface ReposDataProvider {

    Flowable<List<RepoDto>> getRepos();

    Flowable<List<RepoDto>> getReposForUser(String login);
}
