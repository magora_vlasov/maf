package com.magora.maf.model.datasource.users;

import com.magora.maf.application.rest.RestClient;
import com.magora.maf.model.dataobject.dto.UserDto;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public final class UsersRestDataSource implements UsersDataSource {
    private final RestClient restClient;

    public UsersRestDataSource(RestClient restClient) {
        this.restClient = restClient;
    }

    @Override
    public Flowable<List<UserDto>> getUsers() {
        return restClient.getUsers();
    }

    @Override
    public Flowable<UserDto> getUser(String login) {
        return restClient.getUser(login);
    }
}
