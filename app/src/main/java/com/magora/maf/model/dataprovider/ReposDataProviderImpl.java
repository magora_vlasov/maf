package com.magora.maf.model.dataprovider;

import com.magora.maf.model.dataobject.dto.RepoDto;
import com.magora.maf.model.datasource.repos.ReposDataSource;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import io.reactivex.Flowable;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public final class ReposDataProviderImpl implements ReposDataProvider {
    private final ReposDataSource cacheReposDataSource;
    private final ReposDataSource restReposDataSource;

    public ReposDataProviderImpl(ReposDataSource cacheReposDataSource,
                                 ReposDataSource restReposDataSource) {
        this.cacheReposDataSource = cacheReposDataSource;
        this.restReposDataSource = restReposDataSource;
    }

    @Override
    public Flowable<List<RepoDto>> getRepos() {
        return Flowable.fromCallable(() -> {
            final List<RepoDto> repos = new ArrayList<>();
            final AtomicReference<Throwable> throwableReference = new AtomicReference<>();

            cacheReposDataSource.getRepos().subscribe(repos::addAll, throwableReference::set);

            if (repos.isEmpty()) {
                throwableReference.set(null);
                restReposDataSource.getRepos().subscribe(repos::addAll, throwableReference::set);
            }

            if (throwableReference.get() != null) {
                throw new RuntimeException(throwableReference.get());
            }

            return repos;
        });
    }

    @Override
    public Flowable<List<RepoDto>> getReposForUser(String login) {
        return Flowable.fromCallable(() -> {
            final List<RepoDto> repos = new ArrayList<>();
            final AtomicReference<Throwable> throwableReference = new AtomicReference<>();

            cacheReposDataSource.getReposForUser(login).subscribe(repos::addAll, throwableReference::set);

            if (repos.isEmpty()) {
                throwableReference.set(null);
                restReposDataSource.getReposForUser(login).subscribe(repos::addAll, throwableReference::set);
            }

            if (throwableReference.get() != null) {
                throw new RuntimeException(throwableReference.get());
            }

            return repos;
        });
    }
}
