package com.magora.maf.model.validator;

import android.text.TextUtils;

import com.magora.maf.R;
import com.magora.maf.application.manager.ResExtractor;
import com.magora.maf.model.validator.base.Validator;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public class EmptyValidator implements Validator<String> {

    @Override
    public boolean isValid(String value) {
        return !TextUtils.isEmpty(value);
    }

    @Override
    public String getDescription() {
        return ResExtractor.getInstance().getString(R.string.error_validation_empty);
    }
}
