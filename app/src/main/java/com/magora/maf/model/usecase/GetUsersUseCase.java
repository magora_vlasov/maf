package com.magora.maf.model.usecase;

import com.magora.maf.model.dataobject.dto.UserDto;
import com.magora.maf.screen.viewobject.UserVo;
import com.magora.maf.model.dataprovider.UsersDataProvider;
import com.magora.maf.model.usecase.base.BaseLiveUseCase;

import java.util.List;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public final class GetUsersUseCase extends BaseLiveUseCase<Void, List<UserDto>> {
    private final UsersDataProvider usersDataProvider;

    public GetUsersUseCase(UsersDataProvider usersDataProvider) {
        this.usersDataProvider = usersDataProvider;
    }

    @Override
    protected void executeOperation() throws Exception {
        executeOperation(usersDataProvider.getUsers());
    }
}
