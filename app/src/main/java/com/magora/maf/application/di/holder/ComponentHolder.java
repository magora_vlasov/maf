package com.magora.maf.application.di.holder;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public interface ComponentHolder<T> {

    void bindComponent(T component);

    void unbindComponent();

    T getComponent();
}
