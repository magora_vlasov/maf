package com.magora.maf.application.di;

import com.magora.maf.application.di.holder.BaseComponentHolder;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public final class AppComponentHolder extends BaseComponentHolder<AppComponent> {
    private static final AppComponentHolder instance = new AppComponentHolder();

    private AppComponentHolder() {
    }

    public static AppComponentHolder getInstance() {
        return instance;
    }
}
