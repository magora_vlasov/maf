package com.magora.maf.application.di;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public interface DiConsts {
    String TAG = "TAG";
    String KEY_CACHE = TAG + "_KEY_CACHE";
    String KEY_REST = TAG + "_KEY_REST";
}
