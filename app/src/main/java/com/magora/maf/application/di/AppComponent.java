package com.magora.maf.application.di;

import android.content.Context;

import com.magora.maf.model.dataprovider.ReposDataProvider;
import com.magora.maf.model.dataprovider.UsersDataProvider;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
@Singleton
@Component(modules = {AppModule.class, RetrofitModule.class, DataSourceModule.class, DataProviderModule.class})
public interface AppComponent {

    Context provideContext();

    // TODO: Не очень решение, на самом деле. Альтернативы вижу ровно две:
    // 1) Провайдить UseCase'ы в application-wide module
    // 2) В screen-wide component'е прописывать модуль DataProviderModule и при инициации
    // DaggerComponent прописывать его там (беря уже существующий экземпляр)

    UsersDataProvider provideUsersDataProvider();

    ReposDataProvider provideReposDataProvider();
}
