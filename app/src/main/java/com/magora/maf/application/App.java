package com.magora.maf.application;

import android.app.Application;

import com.magora.maf.BuildConfig;
import com.magora.maf.application.di.AppComponentHolder;
import com.magora.maf.application.di.AppModule;
import com.magora.maf.application.di.DaggerAppComponent;
import com.magora.maf.application.manager.ResExtractor;

import io.paperdb.Paper;
import timber.log.Timber;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        initTimber();
        initPaperDb();
        initResExtractor();
        initAppComponent();
    }

    private void initTimber() {
        Timber.plant(BuildConfig.DEBUG ? new Timber.DebugTree() : new TimberReleaseTree());
    }

    private void initPaperDb() {
        Paper.init(this);
    }

    private void initResExtractor() {
        ResExtractor.getInstance().init(this);
    }

    private void initAppComponent() {
        // There is no need to unbind this component, because the system kills the process,
        // accordingly, all objects created by this process are destroyed.
        AppComponentHolder.getInstance().bindComponent(
                DaggerAppComponent.builder()
                        .appModule(new AppModule(this)) // TODO: Заменить на @Component.Builder
                        .build()
        );
    }
}
