package com.magora.maf.application.di.holder;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public abstract class BaseComponentHolder<T> implements ComponentHolder<T> {
    private T component;

    @Override
    public void bindComponent(T component) {
        this.component = component;
    }

    @Override
    public void unbindComponent() {
        this.component = null;
    }

    @Override
    public T getComponent() {
        return component;
    }
}
